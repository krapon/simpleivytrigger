# README #

Jenkins Ivy trigger.

Uses a fully resolved ivy.xml. Will switch rev & revConstraint of every dependency before trying to resolve it again. If any new resolved dependency revision differs from the original ones, a build is triggered.


Based on Ivy Trigger at: https://wiki.jenkins-ci.org/display/JENKINS/IvyTrigger+Plugin