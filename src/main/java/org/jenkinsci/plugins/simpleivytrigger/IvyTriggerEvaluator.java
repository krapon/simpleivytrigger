package org.jenkinsci.plugins.simpleivytrigger;

import hudson.FilePath;
import hudson.remoting.VirtualChannel;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.ivy.Ivy;
import org.apache.ivy.core.report.ResolveReport;
import org.apache.ivy.core.resolve.IvyNode;
import org.apache.ivy.core.resolve.ResolveOptions;
import org.apache.ivy.core.settings.IvySettings;
import org.jenkinsci.lib.xtrigger.XTriggerException;
import org.jenkinsci.lib.xtrigger.XTriggerLog;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.net.URL;
import java.text.ParseException;
import java.util.*;

/**
 * @author Gregory Boissinot
 */
public class IvyTriggerEvaluator implements FilePath.FileCallable<Boolean> {

    private String namespace;

    private FilePath ivyFilePath;

    private FilePath ivySettingsFilePath;

    private final URL ivySettingsURL;

    private FilePath propertiesFilePath;

    private String propertiesContent;

    private XTriggerLog log;

    private boolean debug;

    private Map<String, String> envVars;

    public IvyTriggerEvaluator(String namespace,
                               FilePath ivyFilePath,
                               FilePath ivySettingsFilePath,
                               URL ivySettingsURL,
                               FilePath propertiesFilePath,
                               String propertiesContent,
                               XTriggerLog log,
                               boolean debug,
                               Map<String, String> envVars) {
        this.namespace = namespace;
        this.ivyFilePath = ivyFilePath;
        this.ivySettingsFilePath = ivySettingsFilePath;
        this.ivySettingsURL = ivySettingsURL;
        this.propertiesFilePath = propertiesFilePath;
        this.propertiesContent = propertiesContent;
        this.log = log;
        this.debug = debug;
        this.envVars = envVars;
    }

    public Boolean invoke(File launchDir, VirtualChannel channel) throws IOException, InterruptedException {

        try {
            log.info("Reading Ivy file");
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            Document document = builder.parse(new File(ivyFilePath.getRemote()));

            NodeList nl = document.getDocumentElement().getElementsByTagName("dependencies");

            if (nl == null || nl.getLength() == 0) {
                log.info("No dependencies no trigger");
                return null;
            }

            Element elem = (Element)nl.item(0);
            NodeList deps = elem.getElementsByTagName("dependency");
            if (deps == null || deps.getLength() > 0) {
                log.info("No dependencies no trigger");
                return null;
            }

            Map<String, String> previousRevisions = new HashMap<String, String>();
            for (int i=0; i<deps.getLength(); ++i) {

                Node n = deps.item(i);
                if (n.getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }
                Element dependency = (Element)n;
                NamedNodeMap attributes = dependency.getAttributes();
                if (attributes == null || attributes.getNamedItem("rev") == null || attributes.getNamedItem("revConstraint") == null) {
                    continue;
                }
                String rev = attributes.getNamedItem("rev").getNodeValue();
                String revConstraint = attributes.getNamedItem("revConstraint").getNodeValue();

                if (rev == null || revConstraint == null) {
                    continue;
                }
                attributes.getNamedItem("rev").setNodeValue(revConstraint);
                attributes.removeNamedItem("revConstraint");

                String org = attributes.getNamedItem("org").getNodeValue();
                String module = attributes.getNamedItem("name").getNodeValue();

                previousRevisions.put(org + "." + module, rev);

                log.info("   -- Found: " + org+"."+module  + " revision: " + rev);
            }


            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);
            File tempIvyFile = File.createTempFile("file", ".tmp");

            log.info("Reverting Ivy file: " + tempIvyFile.getAbsolutePath());
            StreamResult modifiedIvy = new StreamResult(tempIvyFile);
            transformer.transform(source, modifiedIvy);


            Ivy ivy = getIvyObject(launchDir, log);
            log.info("\nResolving Ivy dependencies.");
            ResolveOptions options = new ResolveOptions();
            options.setDownload(false);
            ResolveReport resolveReport = ivy.resolve(tempIvyFile.toURI().toURL(), options);
            if (resolveReport.hasError()) {
                List problems = resolveReport.getAllProblemMessages();
                if (problems != null && !problems.isEmpty()) {
                    StringBuilder errorMsgs = new StringBuilder();
                    errorMsgs.append("Errors:\n");
                    for (Object problem : problems) {
                        errorMsgs.append(problem);
                        errorMsgs.append("\n");
                    }
                    log.error(errorMsgs.toString());
                    return null;
                }
            }

            for (Object dependencyObject : resolveReport.getDependencies()) {
                IvyNode dependencyNode = (IvyNode) dependencyObject;

                String rev = dependencyNode.getResolvedId().getRevision();
                String org = dependencyNode.getResolvedId().getOrganisation();
                String module = dependencyNode.getResolvedId().getName();

                String previousRev = previousRevisions.get(org + "." + module);
                if (previousRev != null && !previousRev.equals(rev)) {
                    // Something changed trigger build
                    log.info("\nRevision bump for " + org + "." + module + " revision " +previousRev + " to " + rev );
                    return Boolean.TRUE;
                }
            }
        } catch (ParseException pe) {
            log.error("Parsing error: " + pe.getMessage());
            return null;
        } catch (IOException ioe) {
            log.error("IOException: " + ioe.getMessage());
            return null;
        } catch (XTriggerException xe) {
            log.error("XTrigger exception: " + xe.getMessage());
            return null;
        } catch (ParserConfigurationException e) {
            log.error("ParserConfigurationException exception: " + e.getMessage());
            return null;
        } catch (SAXException e) {
            log.error("SAXException exception: " + e.getMessage());
            return null;
        } catch (TransformerConfigurationException e) {
            log.error("TransformerConfigurationException exception: " + e.getMessage());
            return null;
        } catch (TransformerException e) {
            log.error("TransformerException exception: " + e.getMessage());
            return null;
        }

        return null;
    }

    private Ivy getIvyObject(File launchDir, XTriggerLog log) throws XTriggerException {

        Map<String, String> variables = getVariables();

        File tempSettings = null;
        try {

            //------------ENV_VAR_
            StringBuilder envVarsContent = new StringBuilder();
            for (Map.Entry<String, String> entry : variables.entrySet()) {
                envVarsContent.append(String.format("<property name=\"%s\" value=\"%s\"/>\n", entry.getKey(), entry.getValue()));
            }

            //-----------Inject properties files
            String settingsContent = getIvySettingsContents();
            StringBuilder stringBuffer = new StringBuilder(settingsContent);
            int index = stringBuffer.indexOf("<ivysettings>");
            stringBuffer.insert(index + "<ivysettings>".length() + 1, envVarsContent.toString());
            tempSettings = File.createTempFile("file", ".tmp");
            FileOutputStream fileOutputStream = new FileOutputStream(tempSettings);
            fileOutputStream.write(stringBuffer.toString().getBytes());

            IvySettings ivySettings = new IvySettings();
            ivySettings.load(tempSettings);
            ivySettings.setDefaultCache(getAndInitCacheDir(launchDir));

            Ivy ivy = Ivy.newInstance(ivySettings);
            ivy.getLoggerEngine().pushLogger(new IvyTriggerResolverLog(log, debug));
            for (Map.Entry<String, String> entry : variables.entrySet()) {
                ivy.setVariable(entry.getKey(), entry.getValue());
            }

            return ivy;

        } catch (ParseException pe) {
            throw new XTriggerException(pe);
        } catch (IOException ioe) {
            throw new XTriggerException(ioe);
        } finally {
            if (tempSettings != null) {
                tempSettings.delete();
            }
        }

    }

    /**
     * Method retrieves Ivy Settings contents from URL or from file on
     * master/slave
     * @throws IOException on some IO exception occurs
     */
    private String getIvySettingsContents() throws IOException {
        if (ivySettingsFilePath != null) {
            log.info("Getting settings from file " + ivySettingsFilePath.getRemote());
            return FileUtils.readFileToString(new File(ivySettingsFilePath.getRemote()));
        } else {
            log.info("Getting settings from URL " + ivySettingsURL.toString());
            InputStream is = null;
            try {
                log.info("Getting settings from URL");
                is = ivySettingsURL.openStream();
                return IOUtils.toString(is);
            } finally {
                assert is != null;
                is.close();
            }
        }
    }

    private Map<String, String> getVariables() throws XTriggerException {
        //we want variables to be sorted
        final Map<String, String> variables = new TreeMap<String, String>();
        try {

            //Inject variables from dependencies properties and envVars
            if (envVars != null) {
                variables.putAll(envVars);
            }

            if (propertiesFilePath != null) {

                propertiesFilePath.act(new FilePath.FileCallable<Void>() {
                    public Void invoke(File f, VirtualChannel channel) throws IOException, InterruptedException {
                        Properties properties = new Properties();
                        FileReader fileReader = new FileReader(propertiesFilePath.getRemote());
                        properties.load(fileReader);
                        fileReader.close();
                        for (Map.Entry<Object, Object> entry : properties.entrySet()) {
                            variables.put(String.valueOf(entry.getKey()), String.valueOf(entry.getValue()));
                        }
                        return null;
                    }
                }
                );
            }

            if (propertiesContent != null) {
                Properties properties = new Properties();
                StringReader stringReader = new StringReader(propertiesContent);
                properties.load(stringReader);
                stringReader.close();
                for (Map.Entry<Object, Object> entry : properties.entrySet()) {
                    variables.put(String.valueOf(entry.getKey()), String.valueOf(entry.getValue()));
                }
            }

        } catch (IOException ioe) {
            throw new XTriggerException(ioe);
        } catch (InterruptedException ie) {
            throw new XTriggerException(ie);
        }

        return variables;
    }

    private File getAndInitCacheDir(File launchDir) {
        File cacheDir = new File(launchDir, "ivy-trigger-cache/" + namespace);
        cacheDir.mkdirs();
        return cacheDir;
    }
}
