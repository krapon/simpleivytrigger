package org.jenkinsci.plugins.simpleivytrigger;

import antlr.ANTLRException;
import hudson.Extension;
import hudson.FilePath;
import hudson.Util;
import hudson.console.AnnotatedLargeText;
import hudson.model.AbstractProject;
import hudson.model.Action;
import hudson.model.Node;

import org.apache.commons.jelly.XMLOutput;
import org.jenkinsci.lib.envinject.EnvInjectException;
import org.jenkinsci.lib.envinject.service.EnvVarsResolver;
import org.jenkinsci.lib.xtrigger.*;
import org.jenkinsci.plugins.simpleivytrigger.util.FilePathFactory;
import org.jenkinsci.plugins.simpleivytrigger.util.PropertiesFileContentExtractor;
import org.kohsuke.stapler.DataBoundConstructor;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.*;


/**
 * @author Gregory Boissinot
 */
public class SimpleIvyTrigger extends AbstractTrigger implements Serializable {

    private String ivyPath;

    private String ivySettingsPath;

    private String propertiesFilePath;

    private String propertiesContent;

    private boolean debug;

    @DataBoundConstructor
    public SimpleIvyTrigger(String cronTabSpec, String ivyPath, String ivySettingsPath, String propertiesFilePath, String propertiesContent, LabelRestrictionClass labelRestriction, boolean enableConcurrentBuild, boolean debug) throws ANTLRException {
        super(cronTabSpec, (labelRestriction == null) ? null : labelRestriction.getTriggerLabel(), enableConcurrentBuild);
        this.ivyPath = Util.fixEmpty(ivyPath);
        this.ivySettingsPath = Util.fixEmpty(ivySettingsPath);
        this.propertiesFilePath = Util.fixEmpty(propertiesFilePath);
        this.propertiesContent = Util.fixEmpty(propertiesContent);
        this.debug = debug;
    }

    @SuppressWarnings("unused")
    public String getIvyPath() {
        return ivyPath;
    }

    @SuppressWarnings("unused")
    public String getIvySettingsPath() {
        return ivySettingsPath;
    }

    @SuppressWarnings("unused")
    public String getPropertiesFilePath() {
        return propertiesFilePath;
    }

    @SuppressWarnings("unused")
    public String getPropertiesContent() {
        return propertiesContent;
    }

    @SuppressWarnings("unused")
    public boolean isDebug() {
        return debug;
    }

    @Override
    public Collection<? extends Action> getProjectActions() {
        SimpleIvyTriggerAction action = new InternalSimpleIvyTriggerAction(this.getDescriptor().getDisplayName());
        return Collections.singleton(action);
    }


    public final class InternalSimpleIvyTriggerAction extends SimpleIvyTriggerAction {

        private transient String label;

        public InternalSimpleIvyTriggerAction(String label) {
            this.label = label;
        }

        @SuppressWarnings("unused")
        public AbstractProject<?, ?> getOwner() {
            return (AbstractProject) job;
        }

        public String getIconFileName() {
            return "clipboard.gif";
        }

        public String getDisplayName() {
            return "SimpleIvyTrigger Log";
        }

        public String getUrlName() {
            return "simpleIvyTriggerPollLog";
        }

        @SuppressWarnings("unused")
        public String getLabel() {
            return label;
        }

        @SuppressWarnings("unused")
        public String getLog() throws IOException {
            return Util.loadFile(getLogFile());
        }

        @SuppressWarnings("unused")
        public void writeLogTo(XMLOutput out) throws IOException {
            new AnnotatedLargeText<InternalSimpleIvyTriggerAction>(getLogFile(), Charset.defaultCharset(), true, this).writeHtmlTo(0, out.asWriter());
        }
    }

    private boolean getDependenciesMapForNode(Node launcherNode,
                                                                      XTriggerLog log,
                                                                      FilePath ivyFilePath,
                                                                      FilePath ivySettingsFilePath,
                                                                      URL ivySettingsURL,
                                                                      FilePath propertiesFilePath,
                                                                      String propertiesContent,
                                                                      Map<String, String> envVars) throws IOException, InterruptedException, XTriggerException {

        Boolean result = null;
        if (launcherNode != null) {
            FilePath launcherFilePath = launcherNode.getRootPath();
            if (launcherFilePath != null) {
                result = launcherFilePath.act(new IvyTriggerEvaluator(job.getName(), ivyFilePath, ivySettingsFilePath, ivySettingsURL, propertiesFilePath, propertiesContent, log, debug, envVars));
            }
        }
        return result != null && result;
    }

    /**
     * Method tests, whether the string specifies the local file or an URL. In
     * the second case, URL is returned.
     * @param filename filename to test
     * @param log log for he logging
     * @return URL, if the specified filename is an URL, <code>null</code>
     *         otherwise
     */
    private static URL getRemoteURL(String filename, XTriggerLog log) {
        URL settingsUrl;
        try {
            settingsUrl = new URL(filename);
        } catch (MalformedURLException e) {
            log.info("URL is not well-fotmatted. Assuming it is a local file: " + filename);
            return null;
        }
        final String scheme = settingsUrl.getProtocol();
        if (scheme == null) {
            return null;
        } else {
            return settingsUrl;
        }
    }

    @Override
    protected String getName() {
        return "SimpleIvyTrigger";
    }

    @Override
    protected Action[] getScheduledActions(Node pollingNode, XTriggerLog log) {
        return new Action[0];
    }

    @Override
    protected boolean checkIfModified(Node pollingNode, XTriggerLog log) throws XTriggerException {

        log.info(String.format("Given job Ivy file value: %s", ivyPath));
        log.info(String.format("Given job Ivy settings file value: %s", ivySettingsPath));

        AbstractProject project = (AbstractProject) job;
        EnvVarsResolver varsRetriever = new EnvVarsResolver();
        Map<String, String> envVars;
        try {
            envVars = varsRetriever.getPollingEnvVars(project, pollingNode);
        } catch (EnvInjectException e) {
            throw new XTriggerException(e);
        }

        //Get ivy file and get ivySettings file
        FilePathFactory filePathFactory = new FilePathFactory();
        FilePath ivyFilePath = filePathFactory.getDescriptorFilePath(ivyPath, project, pollingNode, log, envVars);
        final URL ivySettingsUrl = getRemoteURL(ivySettingsPath, log);
        FilePath ivySettingsFilePath = ivySettingsUrl != null ? null : filePathFactory
                .getDescriptorFilePath(ivySettingsPath, project, pollingNode, log, envVars);

        if (ivyFilePath == null) {
            log.error("You have to provide a valid Ivy file.");
            return false;
        }
        if (ivySettingsFilePath == null && ivySettingsUrl == null) {
            log.error("You have to provide a valid IvySettings file or URL.");
            return false;
        }

        log.info(String.format("Resolved job Ivy file value: %s", ivyFilePath.getRemote()));
        log.info(String.format(
                "Resolved job Ivy settings file value: %s",
                ivySettingsUrl == null ? ivySettingsFilePath.getRemote() : ivySettingsUrl
                        .toString()));

        PropertiesFileContentExtractor propertiesFileContentExtractor = new PropertiesFileContentExtractor(new FilePathFactory());
        String propertiesFileContent = propertiesFileContentExtractor.extractPropertiesFileContents(propertiesFilePath, project, pollingNode, log, envVars);
        String propertiesContentResolved = Util.replaceMacro(propertiesContent, envVars);

        boolean hasChanged;
        try {
            FilePath temporaryPropertiesFilePath = pollingNode.getRootPath().createTextTempFile("props", "props", propertiesFileContent);
            log.info("Temporary properties file path is " + temporaryPropertiesFilePath.getName());
            hasChanged = getDependenciesMapForNode(pollingNode, log, ivyFilePath, ivySettingsFilePath, ivySettingsUrl, temporaryPropertiesFilePath, propertiesContentResolved, envVars);
            temporaryPropertiesFilePath.delete();
        } catch (IOException ioe) {
            throw new XTriggerException(ioe);
        } catch (InterruptedException ie) {
            throw new XTriggerException(ie);
        }
        return hasChanged;
    }

    /**
     * Gets the triggering log file
     *
     * @return the trigger log
     */
    protected File getLogFile() {
        return new File(job.getRootDir(), "simple-ivy-polling.log");
    }

    @Override
    protected boolean requiresWorkspaceForPolling() {
        return true;
    }

    @Override
    public String getCause() {
        return "Ivy Dependency trigger";
    }

    @Extension
    @SuppressWarnings("unused")
    public static class IvyScriptTriggerDescriptor extends XTriggerDescriptor {

        @Override
        public String getHelpFile() {
            return "/plugin/simpleivytrigger/help.html";
        }

        @Override
        public String getDisplayName() {
            return "[SimpleIvyTrigger] - Poll with an Ivy script";
        }
    }

}
